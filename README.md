# Dynamic Local Tasks

Create dynamic local tasks for any route without having to create custom modules.

> **NOTE:** Local tasks should typically be defined in a module. This tool is meant to cover special use cases.

## Usage

1. Download and install the `drupal/dynamic_tasks` module. Recommended install method is composer:
   ```
   composer require drupal/dynamic_tasks
   ```
2. Go to /admin/structure/menu/dynamic-tasks to add and manage local tasks.

## Help

See https://www.drupal.org/docs/drupal-apis/menu-api/providing-module-defined-local-tasks for more information
on the available local task configurations.
