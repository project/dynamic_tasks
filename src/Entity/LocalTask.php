<?php

namespace Drupal\dynamic_tasks\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\dynamic_tasks\LocalTaskInterface;

/**
 * Defines the local task configuration entity.
 *
 * @ConfigEntityType(
 *   id = "local_task",
 *   label = @Translation("Local task"),
 *   label_collection = @Translation("Local tasks"),
 *   label_singular = @Translation("Local task"),
 *   label_plural = @Translation("Local tasks"),
 *   label_count = @PluralTranslation(
 *     singular = "@count local task",
 *     plural = "@count local tasks",
 *   ),
 *   handlers = {
 *     "list_builder" = "Drupal\dynamic_tasks\LocalTaskListBuilder",
 *     "form" = {
 *       "default" = "Drupal\dynamic_tasks\LocalTaskForm",
 *       "add" = "Drupal\dynamic_tasks\LocalTaskForm",
 *       "edit" = "Drupal\dynamic_tasks\LocalTaskForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     },
 *   },
 *   admin_permission = "administer dynamic local tasks",
 *   config_prefix = "dynamic_local_task",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "weight" = "weight",
 *   },
 *   links = {
 *     "add-form" = "/admin/structure/menu/dynamic-tasks/add",
 *     "edit-form" = "/admin/structure/menu/dynamic-tasks/manage/{local_task}",
 *     "delete-form" = "/admin/structure/menu/dynamic-tasks/manage/{local_task}/delete",
 *     "collection" = "/admin/structure/menu/dynamic-tasks"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "route_name",
 *     "route_parameters",
 *     "type",
 *     "base_route",
 *     "parent_id",
 *     "weight",
 *   }
 * )
 */
class LocalTask extends ConfigEntityBase implements LocalTaskInterface {

  /**
   * The route name.
   *
   * @var string
   */
  protected string $route_name;

  /**
   * The route parameters.
   *
   * @var string
   */
  protected string $route_parameters;

  /**
   * The type of local task.
   *
   * @var string
   */
  protected string $type;

  /**
   * The base route.
   *
   * @var string
   */
  protected string $base_route;

  /**
   * The parent ID.
   *
   * @var string
   */
  protected string $parent_id;

  /**
   * {@inheritdoc}
   */
  public function getRoute(string $routeName) {
    /** @var \Drupal\Core\Routing\RouteProviderInterface $routeProvider */
    $routeProvider = \Drupal::service('router.route_provider');
    try {
      return $routeProvider->getRouteByName($routeName);
    }
    catch (\Exception $e) {
      return FALSE;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getRouteParameters(): array {
    $params = [];
    foreach (array_filter(preg_split('/\r\n|\r|\n/', $this->route_parameters)) as $param) {
      [$key, $value] = explode('=', $param);
      $params[$key] = $value;
    }
    return $params;
  }

  /**
   * {@inheritdoc}
   */
  public function getParent(string $parentId) {
    /** @var \Drupal\Core\Menu\LocalTaskManagerInterface $localTaskManager */
    $localTaskManager = \Drupal::service('plugin.manager.menu.local_task');
    try {
      return $localTaskManager->getDefinition($parentId);
    }
    catch (\Exception $e) {
      return FALSE;
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function getListCacheTagsToInvalidate(): array {
    return array_merge(parent::getListCacheTagsToInvalidate(), ['local_task']);
  }

}
