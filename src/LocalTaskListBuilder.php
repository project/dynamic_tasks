<?php

namespace Drupal\dynamic_tasks;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;

/**
 * List builder for local task entity.
 *
 * @see \Drupal\dynamic_tasks\Entity\LocalTask
 */
class LocalTaskListBuilder extends ConfigEntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'dynamic_tasks_local_task_list';
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader(): array {
    return [
      $this->t('Title'),
      $this->t('Type'),
      $this->t('Path'),
      $this->t('Weight'),
    ] + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity): array {
    /** @var \Drupal\dynamic_tasks\LocalTaskInterface $entity */
    return [
      $entity->label(),
      $entity->get('type') === 'base_route' ? $this->t('Primary tab') : $this->t('Secondary tab'),
      $entity->getRoute($entity->get('route_name'))->getPath(),
      $entity->get('weight'),
    ] + parent::buildRow($entity);
  }

}
