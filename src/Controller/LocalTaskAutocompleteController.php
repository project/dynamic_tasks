<?php

namespace Drupal\dynamic_tasks\Controller;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Database\Connection;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Provides local tasks route autocomplete controller.
 */
class LocalTaskAutocompleteController extends ControllerBase {

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected Connection $connection;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->connection = $container->get('database');
    return $instance;
  }

  /**
   * Autocomplete callback.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   Returns JSON response of results.
   */
  public function autocomplete(Request $request): JsonResponse {
    if (!($input = Xss::filter($request->query->get('q')))) {
      return new JsonResponse([]);
    }
    $query = $this->connection->select('router')
      ->fields('router', ['name', 'path'])
      ->range(0, 10);
    $input = '%' . $query->escapeLike($input) . '%';
    $query->condition($query->orConditionGroup()
      ->condition('name', $input, 'LIKE')
      ->condition('path', $input, 'LIKE'));
    $query->orderBy('path');

    $items = [];
    foreach ($query->execute() as $result) {
      $items[] = [
        'value' => $result->name,
        'label' => $result->path,
      ];
    }
    return new JsonResponse(array_values($items));
  }

}
