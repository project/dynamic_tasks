<?php

namespace Drupal\dynamic_tasks\Plugin\Derivative;

use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\Discovery\ContainerDeriverInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines dynamic local tasks.
 */
class DynamicLocalTasks extends DeriverBase implements ContainerDeriverInterface {

  /**
   * The local task storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected EntityStorageInterface $storage;

  /**
   * DynamicLocalTasks constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager) {
    $this->storage = $entityTypeManager->getStorage('local_task');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, $base_plugin_id) {
    return new static(
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition): array {
    foreach ($this->storage->loadMultiple() as $entity) {
      /** @var \Drupal\dynamic_tasks\Entity\LocalTask $entity */
      $definition = [
        'title' => $entity->label(),
        'route_name' => $entity->get('route_name'),
        'route_parameters' => $entity->getRouteParameters(),
        'weight' => $entity->get('weight'),
      ];
      switch ($entity->get('type')) {
        case 'base_route':
          $definition['base_route'] = $entity->get('base_route');
          break;

        case 'parent_id':
          $definition['parent_id'] = $entity->get('parent_id');
          break;
      }
      $this->derivatives["dynamic_tasks.{$entity->id()}"] = $definition;
    }
    return parent::getDerivativeDefinitions($base_plugin_definition);
  }

}
