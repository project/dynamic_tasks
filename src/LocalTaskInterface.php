<?php

namespace Drupal\dynamic_tasks;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface defining a local task entity.
 */
interface LocalTaskInterface extends ConfigEntityInterface {

  /**
   * Get the route.
   *
   * @param string $routeName
   *   The route name.
   *
   * @return \Symfony\Component\Routing\Route|false
   *   Returns the matching route. Otherwise, FALSE.
   */
  public function getRoute(string $routeName);

  /**
   * Get route parameters.
   *
   * @return array
   *   Returns an associative array of route parameters.
   */
  public function getRouteParameters(): array;

  /**
   * Get the parent local task.
   *
   * @return array|mixed
   *   Returns the parent local task definition.
   */
  public function getParent(string $parentId);

}
