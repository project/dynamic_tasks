<?php

namespace Drupal\dynamic_tasks;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Render\Markup;
use Drupal\Core\Url;
use Drupal\dynamic_tasks\Entity\LocalTask;

/**
 * The local task entity form.
 *
 * @property \Drupal\dynamic_tasks\LocalTaskInterface $entity
 */
class LocalTaskForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state): array {
    $form = parent::form($form, $form_state);

    $url = 'https://www.drupal.org/docs/drupal-apis/menu-api/providing-module-defined-local-tasks';
    $form['help'] = [
      '#type' => 'details',
      '#title' => $this->t('Help'),
      '#description' => Link::fromTextAndUrl($url, Url::fromUri($url, [
        'attributes' => ['target' => '_blank'],
      ])),
    ];
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Title'),
      '#size' => 30,
      '#required' => TRUE,
      '#default_value' => $this->entity->label(),
    ];
    $form['id'] = [
      '#type' => 'machine_name',
      '#title' => $this->t('ID'),
      '#machine_name' => [
        'exists' => [LocalTask::class, 'load'],
        'source' => ['label'],
      ],
      '#maxlength' => 64,
      '#disabled' => !$this->entity->isNew(),
      '#default_value' => $this->entity->id(),
    ];
    $form['route_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Route name'),
      '#description' => $this->t('Route of the local task link. Search by route name or path.'),
      '#autocomplete_route_name' => 'dynamic_tasks.autocomplete',
      '#required' => TRUE,
      '#default_value' => $this->entity->get('route_name'),
    ];
    $form['route_parameters'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Route parameters'),
      '#description' => $this->t('Define additional route parameters separated by a new line. For example: @list', [
        '@list' => Markup::create('<pre>taxonomy_vocabulary=tags</pre>'),
      ]),
      '#default_value' => $this->entity->get('route_parameters'),
    ];
    $form['type'] = [
      '#type' => 'radios',
      '#title' => $this->t('Type'),
      '#options' => [
        'base_route' => $this->t('Primary tab'),
        'parent_id' => $this->t('Secondary tab'),
      ],
      '#required' => TRUE,
      '#default_value' => $this->entity->get('type'),
    ];
    $form['base_route'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Base route'),
      '#description' => $this->t('Base route of the local task. Search by route name or path.'),
      '#autocomplete_route_name' => 'dynamic_tasks.autocomplete',
      '#default_value' => $this->entity->get('base_route'),
      '#states' => [
        'visible' => [
          ':input[name="type"]' => ['value' => 'base_route'],
        ],
        'required' => [
          ':input[name="type"]' => ['value' => 'base_route'],
        ],
      ],
    ];
    $form['parent_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Parent ID'),
      '#description' => $this->t('Parent ID of the primary local task.'),
      '#default_value' => $this->entity->get('parent_id'),
      '#states' => [
        'visible' => [
          ':input[name="type"]' => ['value' => 'parent_id'],
        ],
        'required' => [
          ':input[name="type"]' => ['value' => 'parent_id'],
        ],
      ],
    ];
    $form['weight'] = [
      '#type' => 'weight',
      '#title' => $this->t('Weight'),
      '#delta' => 50,
      '#default_value' => $this->entity->get('weight') ? $this->entity->get('weight') : 0,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);

    // Verify the route exists and parameters are correctly formatted. Save
    // processed parameters to normalize and strip unnecessary characters.
    if (!$this->entity->getRoute($form_state->getValue('route_name'))) {
      $form_state->setError($form['route_name'], $this->t('Route does not exist.'));
    }
    if ($params = array_filter(preg_split('/\r\n|\r|\n/', $form_state->getValue('route_parameters')))) {
      foreach ($params as &$param) {
        [$key, $value] = explode('=', $param);
        if (!isset($key, $value)) {
          $form_state->setError($form['route_parameters'], $this->t('One or more invalid route parameters.'));
        }
        else {
          $param = "$key=$value";
        }
      }
      $form_state->setValue('route_parameters', implode(PHP_EOL, $params));
    }

    // Verify the base route or parent local task exists.
    switch ($form_state->getValue('type')) {
      case 'base_route':
        if (!$this->entity->getRoute($form_state->getValue('base_route'))) {
          $form_state->setError($form['base_route'], $this->t('Base route does not exist.'));
        }
        $form_state->setValue('parent_id', '');
        break;

      case 'parent_id':
        if (!$this->entity->getParent($form_state->getValue('parent_id'))) {
          $form_state->setError($form['parent_id'], $this->t('Parent local task does not exist.'));
        }
        $form_state->setValue('base_route', '');
        break;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    $this->messenger()->addStatus($this->t('Your settings have been saved.'));
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    parent::save($form, $form_state);
    $form_state->setRedirectUrl($this->entity->toUrl('collection'));
  }

}
